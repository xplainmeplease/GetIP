#include <iostream>
#include <netdb.h>
#include <string>
#include <cstring>
#include <arpa/inet.h>

int main(int argc, char** argv) {

    if(argc < 2) {
        std::cout << "no hostname provided;\n";
        exit(-1);
    }

    char outputIP[INET_ADDRSTRLEN];
    char hostname[128] = ""; // 4 debug only

    addrinfo source; // source
    addrinfo* result; // result
    addrinfo* tempResult; // tmpRes to scan&analyse

    int responseStatus{};

    // ======= init source
    memset(&source, 0, sizeof(source));
    source.ai_family = AF_INET;
    source.ai_socktype = SOCK_STREAM;
    // =============================

    responseStatus = getaddrinfo(hostname, nullptr, &source, &result);

    if(responseStatus != 0)
    {
        std::cout << gai_strerror(responseStatus) << "\n";
    }


    tempResult = result;
    while(tempResult)
    {
        void* tmpAddr;
        if (tempResult->ai_family == AF_INET)
        {
            // IPv4
            auto ipv4 = (sockaddr_in *) tempResult->ai_addr;
            tmpAddr = &(ipv4->sin_addr);
        }
        else
        {
            //IPv6
            auto ipv6 = (sockaddr_in6 *) tempResult->ai_addr;
            tmpAddr = &(ipv6->sin6_addr);
        }

        inet_ntop(tempResult->ai_family, tmpAddr, outputIP, sizeof(outputIP));

        std::cout << outputIP << "\n";
        tempResult = tempResult->ai_next;

    }

    freeaddrinfo(result);

}